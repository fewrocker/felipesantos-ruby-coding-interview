class TweetsController < ApplicationController
  TWEETS_PER_PAGE = 20

  def index
    collection = Tweet.limit(TWEETS_PER_PAGE).order(created_at: :desc)
    collection = collection.where("id < ?", params[:last_item_id]) if params[:last_item_id].present?
    collection = collection.by_username(params[:username]) if params[:username].present?

    render json: collection
  end
end
