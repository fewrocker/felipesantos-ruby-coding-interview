class Tweet < ApplicationRecord
  belongs_to :user

  scope :by_username, -> (username) { joins(:user).where('users.username LIKE ?', "%#{username}%") if username.present? }
end
