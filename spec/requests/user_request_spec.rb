require 'rails_helper'

RSpec.describe "Users", type: :request do

  RSpec.shared_context 'with multiple companies' do
    let!(:company_1) { create(:company) }
    let!(:company_2) { create(:company) }

    before do
      5.times do
        create(:user, company: company_1)
      end
      5.times do
        create(:user, company: company_2)
      end
    end
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching users by company' do
      include_context 'with multiple companies'

      it 'returns only the users for the specified company' do
        get company_users_path(company_1)
        
        expect(result.size).to eq(company_1.users.size)
        expect(result.map { |element| element['id'] } ).to eq(company_1.users.ids)
      end
    end

    context 'when fetching all users' do
      include_context 'with multiple companies'

      context "when no filters are provided" do
        it 'returns all the users' do

        end
      end

      context "when filters are provided" do
        it "returns users with usernames that partially matches the given username" do
          user_with_specific_username = create(:user, company: company_1, username: "k83ja933h")

          get company_users_path(company_1, username: "3ja933")

          expect(result.size).to eq(1)
          expect(result.map { |element| element['id'] } ).to eq([user_with_specific_username.id])
        end
      end
    end
  end
end
